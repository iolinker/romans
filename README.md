# romans

This library contains the methods <code>romanNotation(int num)</code> and 
<code>numeralNotation(String notation)</code>. 

- To make a conversion from any integer number (greatest than zero) to its
roman literal representation use the method <code>romanNotation(int num)</code>.

- To make a conversion from any string (valid roman literal) to its numeric representation
use the method <code>numeralNotation(String notation)</code>.
