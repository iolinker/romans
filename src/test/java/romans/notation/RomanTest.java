package romans.notation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import romans.notation.Roman;

public class RomanTest {
  @Test
  void shouldBeEqualsToEMPTY() {
    assertEquals("",Roman.romanNotation(0));
  }

  @Test
  void shoulBeEqualsToEmptyWhenNegativeInput() {
    assertEquals("",Roman.romanNotation(-3));
  }

  @Test
  void shouldBeEqualsTo3() {
    assertEquals("III",Roman.romanNotation(3));
  }

  @Test
  void shouldBeEqualsTo4() {
    assertEquals("IV",Roman.romanNotation(4));
  }

  @Test
  void shouldBeEqualsTo5() {
    assertEquals("V",Roman.romanNotation(5));
  }

  @Test
  void shouldBeEqualsTo6() {
    assertEquals("VI",Roman.romanNotation(6));
  }

  @Test
  void shouldBeEqualsTo36() {
    assertEquals("XXXVI",Roman.romanNotation(36));
  }

  @Test
  void shouldBeEqualsTo1985() {
    assertEquals("MCMLXXXV",Roman.romanNotation(1985));
  }
}
