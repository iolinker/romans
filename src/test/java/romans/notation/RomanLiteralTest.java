package romans.notation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import romans.notation.Roman;

public class RomanLiteralTest {
  @Test
  void shouldBeEqualsToNegative() {
    assertEquals(-1,Roman.numeralNotation(""));
  }

  @Test
  void shouldBeEqualsToNegativeWhenInvalidLiteral() {
    assertEquals(-1,Roman.numeralNotation("AXBIC"));
  }

  @Test
  void shouldBeEqualsTo3() {
    assertEquals(3,Roman.numeralNotation("III"));
  }

  @Test
  void shouldBeEqualsTo4() {
    assertEquals(4,Roman.numeralNotation("IV"));
  }

  @Test
  void shoulBeEqualsTo10() {
    assertEquals(10,Roman.numeralNotation("X"));
  }

  @Test
  void shouldBeEqualsTo19() {
    assertEquals(19,Roman.numeralNotation("XIX"));
  }

  @Test
  void shouldBeEqualsTo20() {
    assertEquals(20,Roman.numeralNotation("XX"));
  }

  @Test
  void shouldBeEqualsTo40() {
    assertEquals(40,Roman.numeralNotation("XL"));
  }

  @Test
  void shouldBeEqualsTo100() {
    assertEquals(100,Roman.numeralNotation("C"));
  }

  @Test
  void shouldBeEqualsTo1985() {
    assertEquals(1985,Roman.numeralNotation("MCMLXXXV"));
  }

  //Ignoring case test
  @Test
  void shouldBeEqualsTo19IgnoringCase() {
    assertEquals(19,Roman.numeralNotation("xix"));
  }

  //Should NOT be equals tests
  @Test
  void shouldBeInvalidFor9() {
    assertNotEquals(9,Roman.numeralNotation("VIIII"));
  }

  @Test
  void shouldBeInvalidFor90() {
    assertNotEquals(90,Roman.numeralNotation("LXXXX"));
  }

  @Test
  void shouldBeInvalidFor100() {
    assertNotEquals(100,Roman.numeralNotation("LL"));
  }
}
