/**
 * @author JohnnyTB (jtrejosb@live.com)
 * gitlab.com/iolinker | github.com/jtrejosb
 *
 * Licenses GNU GPL v3.0 and Eclipse Public License 2.0
 * Date: 15/08/2022, Time: 18:27:43
 */
package romans.notation;

import java.util.regex.Pattern;

/**
 * Utility class containing handly methods intended to produce a conversion from
 * integer values to a roman notation string and viceversa.
 */
public final class Roman {
  private static final int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
  private static final String[] romanLetters = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};

  /**
   * Deny instantiation for the class.
   */
  private Roman() {}

  /**
   * The conversor method to obtain the roman notation.
   * <ul><li>Given the example value 1985 the result will be MCMLXXXV.
   * <li>If the incoming argument is zero then the returned result will be an
   * empty string.
   * <li>If the incoming argument is less than zero then the returned result also will
   * be an empty string.</ul>
   *
   * @param num any integer value (a zero or negative values are not considered).
   * @return the roman notation of the argument.
   */
  public static String romanNotation(int num) {
    String romanNotation = "";
    if(num>=0) {
      for(int i=0;i<values.length;i++) {
        while(num >= values[i]) {
          num = num - values[i];
          romanNotation+=romanLetters[i];
        }
      }
    }
    return romanNotation;
  }

  /**
   * The conversor method to obtain the numeric value.
   * <p>Each character in the <code>notation</code> string is paired to the index of a integer value
   * in the array <code>values</code> to obtain its equivalent. It will sum the actual value to the last
   * value if the actual is less than the last, otherwise it will subtract when the actual value is greater
   * than the last value.
   * <p>Confirms the conversion comparing the roman literal computed by <code>romanNotation(int num)</code>
   * with the argument <code>notation</code> received by <code>numeralNotation(String notation)</code>
   * <ul><li>Given the example value MCMLXXXV the result will be 1985.
   * <li>Returns -1 if the incoming argument contains not valid characters (only I, V, X, L, C,
   * D and M are valid characters) or if <code>notation</code> is not equals to the roman literal.</ul>
   *
   * @param notation the roman literal to convert to a numeric value. 
   *
   * @return the numeric value of the argument.
   */
  public static int numeralNotation(String notation) {
    int[] values={1,5,10,50,100,500,1000};
    notation=notation.toUpperCase().trim();
    String romans="IVXLCDM";
    int number=0;

    if(!Pattern.matches("[IVXLCDM]+",notation)) return -1;

    int last=values[romans.indexOf(notation.charAt(0))];
    number+=last;
    for(int i=1;i<notation.length();i++) {
      int val=values[romans.indexOf(notation.charAt(i))];
      number+=(val<=last)?val:val-last-last;
      last=val;
    }

    return romanNotation(number).equals(notation)?number:-1;
  }
}
